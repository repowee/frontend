import { createLocalVue, shallowMount as mount } from "@vue/test-utils";
import Vue from "vue";
import VueI18n from "vue-i18n";
import VueMaterial from "vue-material";
import VueRouter from "vue-router";
import initVue from "../../src/init";

const ROOT = createLocalVue();
let COMPONENT = {};

/**
 * Function to initialize vue context
 */
const init = () => {
    Vue.config.silent = true;

    COMPONENT = initVue(ROOT, VueRouter, VueMaterial, VueI18n);
};

/**
 * Function to create a shallow mount
 *
 * @param component Component type
 * @return {Wrapper<Vue>} Wrapper of a Vue component
 */
const shallowMount = (component) => mount(component, { ...COMPONENT });

export { init, shallowMount };
