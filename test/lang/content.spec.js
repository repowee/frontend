import { assert } from "chai";
import lang from "../../src/assets/lang";

/**
 * Function to remove all values inside an object, to keep only the keys
 *
 * @param object Object
 */
const objectWithoutValue = (object) => {
    if (!(object instanceof Object)) {
        return;
    }

    for (const key of Object.keys(object)) {
        if (object[key] instanceof Object) {
            objectWithoutValue(object[key]);
        } else {
            object[key] = "NO_VALUE_FOR_TESTING_PURPOSE";
        }
    }
};

describe("Lang", () => {
    it("Same content in all languages", () => {
        objectWithoutValue(lang.en);
        objectWithoutValue(lang.fr);

        assert.deepEqual(lang.en, lang.fr);
    });
});
