import { assert } from "chai";
import App from "../../src/App";
import { init, shallowMount } from "../test-utils/vue";

// Init vue context
init();

const component = () => shallowMount(App);

describe("App", () => {
    describe("#checkElements()", () => {
        it("Should have an <div id='app'>", () => {
            const app = component();
            const element = app.find("div");

            assert.isOk(app.exists());
            assert.equal(element.attributes("id"), "app");
        });

        it("Should have a router in <div>", () => {
            const app = component();
            const root = app.find("div");

            assert.isOk(root.exists());
            assert.isOk(root.find("router-view").exists());
        });
    });
});
