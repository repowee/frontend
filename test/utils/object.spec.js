import { assert } from "chai";
import { merge } from "../../src/utils/object";

describe("ObjectUtils", () => {
    describe("#merge", () => {
        it("Should handle undefined/null values", () => {
            assert.deepEqual({}, merge(undefined, undefined));
            assert.deepEqual({}, merge(null, null));
            assert.deepEqual({ a: "a" }, merge({ a: "a" }, undefined));
            assert.deepEqual({ b: "b" }, merge({ b: "b" }, null));
        });

        it("Should merge empty objects", () => {
            const obj = {
                test: {
                    a: "a"
                }
            };

            assert.deepEqual(obj, merge({}, obj));
            assert.deepEqual(obj, merge({ ...obj }, {}));
        });

        it("Should correctly merge objects", () => {
            const obj = {
                test: {
                    a: "a"
                }
            };

            const obj2 = {
                test: {
                    b: {
                        value: "b"
                    }
                },
                test_2: {
                    value: "c"
                }
            };

            assert.deepEqual({
                test: {
                    a: "a",
                    b: {
                        value: "b"
                    }
                },
                test_2: {
                    value: "c"
                }
            }, merge(obj, obj2));
        });
    });
});
