import VueI18n from "vue-i18n";
import VueRouter from "vue-router";
import language from "./assets/lang";
import routes from "./routes";

/**
 * Function to initialize vue context
 *
 * @param instance Vue instance
 * @param plugins Plugins to activate
 * @return {{}} Components
 */
const init = (instance, ...plugins) => {
    // Import plugins
    plugins.forEach(p => instance.use(p));

    // Components
    const components = {};

    // I18 - Language
    components.i18n = new VueI18n({
        messages: language, // set locale messages
        locale: (navigator.language.includes("-") ? navigator.language.split("-")[0] : navigator.language), // set locale
        fallbackLocale: "en",
        availableLocales: [ "en", "fr" ],
        silentFallbackWarn: true
    });

    // Create router
    components.router = new VueRouter({
        mode: "history",
        routes: routes
    });

    return components;
};

export default init;
