/**
 * Function to check if application is in production mode
 *
 * @return {boolean} Mode is set to production
 */
const isProduction = () => {
    return process.env.NODE_ENV === "production";
};

export { isProduction };
