import { merge } from "../../utils/object";
// Lang files
import connection from "./connection";
import footer from "./footer";
import global from "./global";
import header from "./header";
import profile from "./profile";
import home from "./home";
import workspaces from "./workspaces.json";
import workspace from "./workspace.json";
import project from "./project.json";

/**
 * DO NOT FORGET TO ADD YOUR NEW LANGUAGE FILE TO MERGE PARAMETERS
 */

export default merge(connection, footer, global, header, home, profile, workspaces, workspace, project);
