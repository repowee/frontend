import Connection from "../components/Connection";
import Home from "../components/Home";
import Profile from "../components/Profile";
import Project from "../components/Project";
import Workspace from "../components/Workspace";
import Workspaces from "../components/Workspaces";

export default [
    {
        path: "/",
        name: "connection",
        component: Connection
    },
    {
        path: "/home",
        name: "home",
        component: Home
    },
    {
        path: "/profile",
        name: "profile",
        component: Profile
    },
    {
        path: "/workspaces",
        name: "workspaces",
        component: Workspaces
    },
    {
        path: "/:user/workspace/:w_name",
        name: "workspace",
        component: Workspace
    },
    {
        path: "/:user/workspace/:w_name/project/:p_name",
        name: "project",
        component: Project
    }
];
