import Vue from "vue";
import VueI18n from "vue-i18n";
import VueMaterial from "vue-material";
import "vue-material/dist/theme/default.css";
import "vue-material/dist/vue-material.min.css";
import VueRouter from "vue-router";
import App from "./App";
import Vuelidate from "vuelidate";
import VueCookies from "vue-cookies";

import init from "./init";
import axios from "./common/axios";

// Init
const components = init(Vue, VueRouter, VueMaterial, VueI18n, Vuelidate, VueCookies);

components.router.beforeEach((to, from, next) => {
    axios.get(process.env.VUE_APP_BACKEND_URL + "v1/auth/verify").then((response) => {
        if (to.name === "connection") {
            next({ name: "home" });
        } else {
            next();
        }
    }).catch((errors) => {
        if (to.name === "connection") {
            next();
        } else {
            next({ name: "connection" });
        }
    });
});

new Vue({
    router: components.router,
    i18n: components.i18n,
    render: h => h(App)
}).$mount("#app");
