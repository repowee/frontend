module.exports = {
    publicPath: process.env.NODE_ENV === "development" ? "http://localhost:8080" : "/",
    configureWebpack: {
        devServer: {
            historyApiFallback: true
        }
    }
};
